# Import libs
import os
from pathlib import Path

import toml
import click
from dotenv import dotenv_values


# Use click to get arguments
@click.command()
@click.argument('working_dir')
def main(working_dir):
    if not os.path.isdir(working_dir):
        print(working_dir + ' does not exists')
        exit(1)
    file_input = Path(working_dir) / 'project' / '.env'
    file_output = Path(working_dir) / 'customer_settings.toml'
    env_parsed = dotenv_values(stream=file_input)
    processed_dictionary = {}
    for key, value in env_parsed.items():
        try:
            value = int(value)
        except ValueError:
            try:
                value = float(value)
            except ValueError:
                pass
        processed_dictionary[key] = value
    post_processed_dictionary = {'default': processed_dictionary}
    with open(file_output, 'w') as file_output:
        toml.dump(post_processed_dictionary, file_output)


if __name__ == '__main__':
    main()
