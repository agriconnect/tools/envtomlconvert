Usage:
envtomlconvert.py /path/to/installation_dir

Tools to migrate PlantingHoue settings from _.env_ file to _customer_settings.toml_.
